#!/bin/bash

mkdir -p target/bin/plugins
mkdir -p target/lib

cd plugin-build
./build-old.sh
cd ..

cd plugin-collect-sources
./build-old.sh
cd ..

cd plugin-debian
./build-old.sh
cd ..

cd plugin-init
./build-old.sh
cd ..

cd plugin-repository
./build-old.sh
cd ..

cd plugin-uncrustify
./build-old.sh
cd ..
