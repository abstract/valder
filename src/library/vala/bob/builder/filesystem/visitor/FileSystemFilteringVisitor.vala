namespace bob.builder.filesystem.visitor {

	protected abstract class FileSystemFilteringVisitor : FileSystemVisitor {

		private FileFilter filter;

		protected FileSystemFilteringVisitor(FileFilter filter) {
			this.filter = filter;
		}

		protected abstract void visitFileFiltered(File file);

		public void visitFile(File file) {
			if (filter.fileMatchesCriteria(file)) {
				visitFileFiltered(file);
			}
		}

		public void visitDirectory(File directory) {}
	}
}
