#!/bin/bash

[ -d "target/bin/plugins" ] && rm -rf target/bin/plugins/*.so

mkdir -p target/bin/plugins
cp plugin-*/target/lib/libplugin-*.so target/bin/plugins/
