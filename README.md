# valder
Valder - Vala source code welder

Please refer to http://activey.github.io/valder

## Setup & Installation

This application requires code/packages/includes that are not currently installed with Vala. This requires us to build vala locally and install it as such.


* Step 1 - Install Basic Vala + Toolchains
```
sudo apt-get install valac-0.48 libvala-0.48-dev build-essential git libgee-0.8-dev libjson-glib-1.0 libjson-glib-dev autoconf libtool bison flex autoconf-archive
```

* Step 2 - Copy Vapi Files.
```
sudo cp vala-vapi/ccode.vapi /usr/share/vala/vapi/
sudo cp vala-vapi/codegen.vapi /usr/share/vala/vapi/
```

* Step 3 - Copy Header Files.
```
sudo cp vala-vapi/valaccode.h /usr/include/vala-${YOURVERSIONHERE}/
sudo cp vala-vapi/valacodegen.h /usr/include/vala-${YOURVERSIONHERE}/
```

* Step 6 - Build the tool!

```
# Compile Library
./build-library.sh

# Compile Application
./build-cmdline.sh

# Compile Plugins
./build-plugins.sh
```
